# ember-heroicons-bugreport

## Steps to reproduce

### Manual

1. Bootstrap new app:
```sh
EMBER_CLI_PNPM=true ember new ember-heroicons-bugreport --skip-npm --pnpm=true --typescript=true
```
2. Install `ember-heroicons`:
```sh
ember install ember-heroicons
```
3. Try to render any `HeroIcon` component:
```hbs
<HeroIcon @icon="check-circle" />
```

### Prepared

1. Clone this repo
2. Install:
```sh
pnpm install
```
3. Run:
```sh
pnpm start
```
4. Open: http://localhost:4200/
5. See that respective place where hero icon should be is empty:
```html
<p>
  -- START --
</p>
<svg>
    
</svg>
<p>
  -- END --
</p>
```
